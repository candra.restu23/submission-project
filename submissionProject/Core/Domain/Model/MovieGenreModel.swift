//
//  MovieGenreModel.swift
//  submissionProject
//
//  Created by Candra Restu Noviar on 07/02/24.
//

import Foundation
struct MovieGenreModel {
    var id: Int?
    var name: String?
}

typealias MovieGenreListModel = [MovieGenreModel]
