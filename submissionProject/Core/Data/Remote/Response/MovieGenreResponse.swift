//
//  MovieGenreResponse.swift
//  submissionProject
//
//  Created by Candra Restu Noviar on 07/02/24.
//

import Foundation

struct MovieGenreResponse: Codable {
    var id: Int?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}

struct MovieGenreListResponse: Codable {
    let genres: [MovieGenreResponse]?
    
    enum CodingKeys: String, CodingKey {
        case genres
    }
}
