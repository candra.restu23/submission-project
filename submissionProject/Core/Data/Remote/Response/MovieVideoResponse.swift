//
//  MovieVideoResponse.swift
//  submissionProject
//
//  Created by Candra Restu Noviar on 07/02/24.
//

import Foundation

struct MovieVideoResponse: Codable {
    let iso639: String?
    let iso3166: String?
    let name, key: String?
    let site: String?
    let size: Int?
    let type: String?
    let official: Bool?
    let publishedAt, id: String?

    enum CodingKeys: String, CodingKey {
        case iso639 = "iso_639_1"
        case iso3166 = "iso_3166_1"
        case name, key, site, size, type, official
        case publishedAt = "published_at"
        case id
    }
}

struct MovieVideoListResponse: Codable {
    let results: [MovieVideoResponse]?
    
    enum CodingKeys: String, CodingKey {
        case results
    }
}
typealias MovieVideosResponse = [MovieVideoResponse]
