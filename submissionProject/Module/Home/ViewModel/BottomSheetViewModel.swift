//
//  BottomSheetViewModel.swift
//  submissionProject
//
//  Created by candra restu on 21/05/22.
//

import Foundation
import RxSwift
import RxCocoa

class BottomSheetViewModel: ObservableObject {
    private let disposeBag = DisposeBag()
    private var homeUseCase: HomeUseCase?
    let isLoading = ObservableData<Bool>()
    let errorMessage = ObservableData<String>()

    let data = BehaviorRelay<MovieGenreListModel>(value: [])
    let categoryName = ["popular", "upcoming", "top rated", "now playing"]
    let categoryID = ["popular", "upcoming", "top_rated", "now_playing"]

    
    init(homeUseCase: HomeUseCase?) {
        self.homeUseCase = homeUseCase
    }
    
    func setupData() {
//        data.accept(categoryName)
    }
    
    func getMovies() {
        isLoading.value = true
        homeUseCase?.getGenres()
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] result in
                self?.data.accept(result)
            } onError: { [weak self] error in
                self?.errorMessage.value = error.localizedDescription
            } onCompleted: {
                self.isLoading.value = false
            }.disposed(by: disposeBag)
    }
}
