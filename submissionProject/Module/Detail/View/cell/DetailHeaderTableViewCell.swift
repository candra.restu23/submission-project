//
//  DetailHeaderTableViewCell.swift
//  submissionProject
//
//  Created by Candra Restu Noviar on 07/02/24.
//

import UIKit
import youtube_ios_player_helper

class DetailHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewDescLabel: UILabel!
    @IBOutlet weak var youtubePlayerView: YTPlayerView!
    
    func setupCell(_ data: MovieDetailModel,_ youtubeID: String) {
        let imageUrl = URL(string: "\(Constant.BaseImage)\(data.posterPath ?? "")")
        movieImage.sd_setImage(with: imageUrl)
        titleLabel.text = data.title ?? ""
        releaseDateLabel.text = data.releaseDate ?? ""
        overviewDescLabel.text = data.overview ?? ""
        youtubePlayerView.load(withVideoId: youtubeID)
    }
    
}
