//
//  ReviewTableViewCell.swift
//  submissionProject
//
//  Created by candra restu on 21/05/22.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    func setupCell(_ item: MovieReviewResultModel) {
        let imageUrl = URL(string: "\(Constant.BaseImage)\(item.authorDetails?.avatarPath ?? "")")
        userImage.sd_setImage(
            with: imageUrl,
            placeholderImage: UIImage(systemName: "person.fill"))
        userNameLabel.text = item.authorDetails?.username ?? ""
        reviewLabel.text = item.content ?? ""
    }
    
}
