//
//  DetailViewController.swift
//  submissionProject
//
//  Created by candra restu on 20/05/22.
//

import UIKit
import RxCocoa
import RxSwift
import SDWebImage

class DetailViewController: UIViewController {
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    var viewModel: DetailViewModel?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bindData()
        if viewModel?.isFavorite ?? false {
            viewModel?.data.value = viewModel?.localData
            viewModel?.isEmpty.value = true
            viewModel?.isLoading.value = false
        } else {
            viewModel?.getMovieVideo()
            viewModel?.getMovieReview()
        }
        setupTableView()
        setupNavBar()
    }
    
    private func setupViews() {
//        cardView.dropShadow()
//        loadingView.startAnimating()
//        setupFavoriteView()
//        let tapGestureRecognizer = UITapGestureRecognizer(
//            target: self,
//            action: #selector(imageTapped(tapGestureRecognizer:))
//        )
//        favoriteImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func bindData() {
        viewModel?.data.observe(disposeBag) { [weak self] (data) in
            guard let data = data else { return }
            self?.reviewTableView.reloadData()
//            let imageUrl = URL(string: "\(Constant.BaseImage)\(data.posterPath ?? "")")
//            self?.movieImage.sd_setImage(with: imageUrl)
//            self?.titleLabel.text = data.title ?? ""
//            self?.releaseDateLabel.text = data.releaseDate ?? ""
//            self?.overViewDescLabel.text = data.overview ?? ""
//            self?.title = data.title ?? ""
        }
        
        viewModel?.reviewData.observe(disposeBag) { [weak self] data in
            guard let data else { return }
        }
        
//        viewModel?.reviewData
//            .bind(to: reviewTableView.rx.items(
//                cellIdentifier: "ReviewCell",
//                cellType: ReviewTableViewCell.self)) { _, item, cell in
//                    let imageUrl = URL(string: "\(Constant.BaseImage)\(item.authorDetails?.avatarPath ?? "")")
//                    cell.userImage.sd_setImage(
//                        with: imageUrl,
//                        placeholderImage: UIImage(systemName: "person.fill"))
//                    cell.userNameLabel.text = item.authorDetails?.username ?? ""
//                    cell.reviewLabel.text = item.content ?? ""
//                }.disposed(by: disposeBag)
        
        viewModel?.isLoading.observe(disposeBag) { [weak self] isLoading in
            guard let isLoading = isLoading else { return }
            self?.loadingView.isHidden = !isLoading
        }
        
        viewModel?.isEmpty.observe(disposeBag) { [weak self] isEmpty in
            guard let isEmpty = isEmpty else { return }

        }
        
        viewModel?.isSuccessSave.observe(disposeBag) { [weak self] _ in
            guard let alert = self?.showSaveAlert(
                message: "This movie is now in your favorite"
            ) else { return }
            self?.present(alert, animated: true, completion: nil)
            self?.viewModel?.isFavorite = true
            self?.setupNavBar()
        }
        
        viewModel?.isSuccessDelete.observe(disposeBag) { [weak self] _ in
            guard let alert = self?.showSaveAlert(
                message: "This movie is deleted from your favorite"
            ) else { return }
            self?.present(alert, animated: true, completion: nil)
            self?.viewModel?.isFavorite = false
            self?.setupNavBar()
        }
        
        viewModel?.errorMessage.observe(disposeBag) { [weak self] errorMessage in
            guard let alert = self?.showErrorAlert(
                errorMessage: errorMessage ?? ""
            ) else { return }
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func setupTableView() {
        let nibName = UINib(nibName: "ReviewTableViewCell", bundle: nil)
        let nibHeaderName = UINib(nibName: "DetailHeaderTableViewCell", bundle: nil)
        reviewTableView.register(nibName, forCellReuseIdentifier: "ReviewCell")
        reviewTableView.register(nibHeaderName, forCellReuseIdentifier: "DetailHeaderTableViewCell")
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
    }
    
    private func setupNavBar() {
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(labelTapped(tapGestureRecognizer:))
        )
        
        var label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = .blue
        if viewModel?.isFavorite ?? false {
            label.text = "remove from favorite"
        } else {
            label.text = "save to favorite"
        }
        label.addGestureRecognizer(tapGestureRecognizer)
        let item = UIBarButtonItem(customView: label)
        self.navigationItem.rightBarButtonItem = item
    }
    
    @objc func labelTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if viewModel?.isFavorite ?? false {
            viewModel?.deleteMovies()
        } else {
            viewModel?.saveMovie()
        }
    }
    
    func showErrorAlert(errorMessage: String) -> UIAlertController {
        let refreshAlert = UIAlertController(
            title: "Network Error",
            message: "Something work with the netwok, try again later!",
            preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(
            UIAlertAction(title: "Ok",
                          style: .default,
                          handler: { ( _ : UIAlertAction!) in

        }))

        return refreshAlert
    }
    
    func showSaveAlert(message: String) -> UIAlertController {
        let refreshAlert = UIAlertController(
            title: "Success!",
            message: message,
            preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(
            UIAlertAction(title: "Ok",
                          style: .default,
                          handler: { ( _ : UIAlertAction!) in

        }))

        return refreshAlert
    }
}

extension DetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return viewModel?.reviewData.value?.count ?? 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "DetailHeaderTableViewCell",
                for: indexPath) as? DetailHeaderTableViewCell else { return UITableViewCell() }
            if let data = viewModel?.data.value, let youtubeID = viewModel?.youtubeID {
                cell.setupCell(data, youtubeID)
            }
            return cell
            
        default:
            if (viewModel?.reviewData.value?.count ?? 0) > 0 {
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "ReviewCell",
                    for: indexPath) as? ReviewTableViewCell else { return UITableViewCell() }
                
                if let data = viewModel?.reviewData.value?.at(index: indexPath.row) {
                    cell.setupCell(data)
                }
                return cell
            } else {
                let cell = UITableViewCell(frame:
                                            CGRect(x: 0,
                                                   y: 0,
                                                   width: tableView.frame.width,
                                                   height: 300))
                let label = UILabel(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: tableView.frame.width,
                                                  height: 300))
                label.text = "Data Not Available"
                label.textAlignment = .center
                cell.contentView.addSubview(label)
                return cell
            }

        }
    }
}

extension DetailViewController: UITableViewDelegate {
    
}
